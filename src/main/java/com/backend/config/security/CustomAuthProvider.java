package com.backend.config.security;

import com.backend.dao.UserRepoCustom;
import com.backend.model.entity.account.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rasmus on 2016-06-03.
 */
public class CustomAuthProvider implements AuthenticationProvider {

    @Autowired
    private UserRepoCustom repo;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        TokenWrapper tokenWrapper = (TokenWrapper) authentication;
        String token = authentication.getCredentials().toString();

        User user = null;

        if (tokenWrapper.isSwaggerAuth()) {
            user = repo.findBySwaggerKey(token);
        } else if (token != null) {
            user = repo.findByToken(token);
        }

        if (user != null) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority(user.getRole()));
            Authentication auth = new UsernamePasswordAuthenticationToken(user,token, grantedAuths);
            return auth;
        }

        throw new BadCredentialsException("Dat is no good");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(TokenWrapper.class);
    }
}
