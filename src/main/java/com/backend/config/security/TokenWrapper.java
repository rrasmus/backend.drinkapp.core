package com.backend.config.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by rasmus on 2016-06-06.
 */
public class TokenWrapper implements Authentication {

    private String token;
    private Boolean isSwaggerAuth;

    public TokenWrapper(String token,Boolean isSwagger) {
        this.token = token;
        this.isSwaggerAuth = isSwagger;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }

    public Boolean isSwaggerAuth() {
        return isSwaggerAuth;
    }

    public void setIsSwaggerAuth(Boolean swaggerAuth) {
        isSwaggerAuth = swaggerAuth;
    }
}