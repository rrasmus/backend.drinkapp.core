package com.backend.config.security;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.backend.util.Resources;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Created by rasmus on 2016-06-03.
 */
public class CustomAuthFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {


        String xAuth = request.getHeader(Resources.SecurityResources.AUTHORIZATION_HEADER_NAME);
        String swaggerApiKey = request.getParameter(Resources.SecurityResources.SWAGGER_PARAMETER_NAME);

        if (xAuth != null) {
            Authentication auth = new TokenWrapper(xAuth,false);
            SecurityContextHolder.getContext().setAuthentication(auth);
        } else if (swaggerApiKey != null) {
            Authentication auth = new TokenWrapper(swaggerApiKey,true);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        else {
            // throw something?
        }

        filterChain.doFilter(request, response);
    }



}