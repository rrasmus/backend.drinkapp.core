package com.backend.config.aws;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rasmus on 2016-06-11.
 */
@Configuration
public class AwsResources {

    public static class S3 {
        @Value("${BUCKET_NAME_TEMPLATE}")
        public static  String BUCKET_NAME_TEMPLATE;

        @Value("${BUCKET_NAME_USER}")
        public static  String BUCKET_NAME_USER;

        public static final String USER_PHOTO_MAP_NAME = "photos";
    }

    public static class SQS {
        @Value("${SQS_VISION_QUEUE_URL}")
        public static String QUEUE_URL;
        public static final String MESSAGE_TYPE_KEY = "key";
    }

}
