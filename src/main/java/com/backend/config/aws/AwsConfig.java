package com.backend.config.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.backend.worker.AwsSqsReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Created by rasmus on 2016-06-11.
 */
@Configuration
public class AwsConfig {

    @Value("${AWS_KEY}")
    private String key;
    @Value("${AWS_VALUE}")
    private String value;

    @Bean
    public AWSCredentials getProvider() {
        return new BasicAWSCredentials(key,value);
    }
}
