package com.backend.config.aws.sqs;

import org.json.JSONObject;

import static com.backend.config.aws.AwsResources.SQS.MESSAGE_TYPE_KEY;

/**
 * Created by rasmus on 12/27/16.
 */
public class TemplateMessageSqs {
    public static final String TYPE = "template";
    public static final String METHOD_KEY = "message_method";
    public static final String ID_KEY = "id";
    public static final String LINK_KEY = "link";

    private JSONObject jsonObject;

    public static final int METHOD_NEW = 0;
    public static final int METHOD_UPDATE = 1;
    public static final int METHOD_DELETE = 2;


    public TemplateMessageSqs() {
        jsonObject = new JSONObject();
        jsonObject.put(MESSAGE_TYPE_KEY,TYPE);
    }

    public void setMethod(int method) {
        jsonObject.put(METHOD_KEY,method);
    }

    public void setLink(String link) {
        jsonObject.put(LINK_KEY,link);
    }

    public void setId(Long id) {
        jsonObject.put(ID_KEY,id);
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}