package com.backend.config.aws.sqs;

import org.json.JSONObject;

import static com.backend.config.aws.AwsResources.SQS.MESSAGE_TYPE_KEY;

/**
 * Created by rasmus on 12/27/16.
 */
public class PhotoMessageSqs {
    public static final String TYPE = "photo";
    public static final String PHOTO_ID_KEY = "photo_id";
    public static final String LINK_KEY = "link";

    private JSONObject jsonObject;

    public PhotoMessageSqs() {
        jsonObject = new JSONObject();
        jsonObject.put(MESSAGE_TYPE_KEY,TYPE);
    }

    public void setPhotoId(Long id) {
        jsonObject.put(PHOTO_ID_KEY,id);
    }

    public void setLink(String link) {
        jsonObject.put(LINK_KEY,link);
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}
