package com.backend.dao;

import com.backend.model.entity.api.PartIngredient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rasmus on 2016-05-14.
 */
@Repository
public interface PartRepo extends CrudRepository<PartIngredient,Long> {
}
