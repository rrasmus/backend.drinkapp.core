package com.backend.dao;

import com.backend.model.entity.api.Photo;
import com.backend.model.entity.api.Recipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rasmus on 12/26/16.
 */
@Repository
public interface PhotoRepo extends CrudRepository<Photo,Long> {
}
