package com.backend.dao;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rasmus on 6/15/16.
 */
@Repository
@Transactional
public class SearchRepo {

    public enum OrderSearchBy {
        MAX_MATCH,MIN_MATCH
    }

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @PostConstruct
    public void init() {
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }


    public List<SearchSet> getRecipes(List<Integer> ingredientList, List<Integer> includedCategories, OrderSearchBy orderSearchBy) {
        String ingredQuerySet = buildQuerySet(ingredientList);

        String query = "SELECT COALESCE(ct.cat_count_tot,0) - COALESCE(ct.cat_count_hits,0) + COALESCE(ctt.ingred_count_misses,0) AS total_misses,\n" +
                "ct.cat_hits,\n" +
                "ctt.ingred_misses,\n" +
                "r.*,\n" +
                "u.username,u.role\n" +
                "FROM Recipe r\n" +
                "LEFT JOIN (\n" +
                "\tSELECT recipe_id,COUNT(pi.ingredient_id) as ingred_count_misses,GROUP_CONCAT(DISTINCT pi.ingredient_id SEPARATOR ',') AS ingred_misses FROM PartIngredient pi WHERE pi.ingredient_id NOT IN " + ingredQuerySet + "\n" +
                "\tGROUP BY pi.recipe_id\n" +
                "\t) ctt ON ctt.recipe_id = r.id\n" +
                "LEFT JOIN (\n" +
                "\tSELECT recipe_id,COUNT(DISTINCT pc.category_id) as cat_count_tot,COUNT(DISTINCT icc.categorySet_id) as cat_count_hits, GROUP_CONCAT(DISTINCT icc.categorySet_id SEPARATOR ',') AS cat_hits FROM PartCategory pc\n" +
                "\tLEFT JOIN Ingredient_Category icc on icc.categorySet_id = pc.category_id and icc.ingredientSet_id in " + ingredQuerySet +"\n" +
                "\tGROUP BY recipe_id\n" +
                "\t) ct ON ct.recipe_id=r.id\n" +
                "LEFT JOIN User u on u.id = r.author_id\n";

        if (includedCategories != null) {
            String catQuerySet = buildQuerySet(includedCategories);
            query += "WHERE (SELECT count(*) from PartCategory pc where pc.recipe_id = r.id and pc.category_id in " + catQuerySet + ") > 0\n";
        }

        query += "GROUP BY r.id\n";

        if (orderSearchBy != null) {
            switch (orderSearchBy) {

                case MAX_MATCH:
                    query += "ORDER BY total_misses ASC";
                    break;
                case MIN_MATCH:
                    query += "ORDER BY total_misses DESC";
                    break;
            }
        } else {
            query += "ORDER BY total_misses ASC";
        }

        List<SearchSet> set = jdbcTemplateObject.query(query,new SearchSetMapper());
        return set;
    }


    private String buildQuerySet(List<Integer> intList) {
        String querySet = "(";

        for (int i = 0; i < intList.size();i++) {
            querySet += intList.get(i);
            if (i < intList.size()-1) {
                querySet += ",";
            }
        }

        querySet += ")";
        return querySet;
    }

    class SearchSetMapper implements RowMapper<SearchSet> {

        @Override
        public SearchSet mapRow(ResultSet resultSet, int i) throws SQLException {
            SearchSet ss = new SearchSet();
            ss.setTotalMisses(resultSet.getInt("total_misses"));
            ss.setRecipeId(resultSet.getInt("id"));

            // result is return in format ["a","b"] -> split it up and add to array
            String catStr = resultSet.getString("cat_hits");
            if (catStr != null) {
                String[] catHits = catStr.split(",");
                ArrayList<Integer> catHitsList = new ArrayList<>();

                for (String cat : catHits) {
                    catHitsList.add(Integer.parseInt(cat));
                }
                ss.setCategoryHits(catHitsList);
            } else {
                ss.setCategoryHits(new ArrayList<>());
            }

            String ingredStr = resultSet.getString("ingred_misses");

            if (ingredStr != null) {
                String[] ingredHits = resultSet.getString("ingred_misses").split(",");
                ArrayList<Integer> ingredHitsList = new ArrayList<>();

                for (String ingred : ingredHits) {
                    ingredHitsList.add(Integer.parseInt(ingred));
                }
                ss.setIngredientMisses(ingredHitsList);
                ss.setRecipeId(resultSet.getInt("id"));
            } else {
                ss.setIngredientMisses(new ArrayList<>());
            }

            ss.setName(resultSet.getString("name"));
            ss.setUsername(resultSet.getString("username"));
            ss.setRole(resultSet.getString("role"));
            return ss;
        }
    }


    class SearchSet {

        private int recipeId;
        private int totalMisses;
        private List<Integer> categoryHits;
        private List<Integer> ingredientMisses;
        private String name;
        private int userId;
        private String username;
        private String role;

        public SearchSet() {

        }

        public SearchSet(int recipeId,int totalMisses,List<Integer> categoryHits,List<Integer> categoryTotal,List<Integer> ingredientMisses,String name) {
            this.recipeId = recipeId;
            this.totalMisses = totalMisses;
            this.categoryHits = categoryHits;
            this.ingredientMisses = ingredientMisses;
            this.name = name;
        }

        public int getRecipeId() {
            return recipeId;
        }

        public void setRecipeId(int recipeId) {
            this.recipeId = recipeId;
        }

        public int getTotalMisses() {
            return totalMisses;
        }

        public void setTotalMisses(int totalMisses) {
            this.totalMisses = totalMisses;
        }

        public List<Integer> getCategoryHits() {
            return categoryHits;
        }

        public void setCategoryHits(List<Integer> categoryHits) {
            this.categoryHits = categoryHits;
        }

        public List<Integer> getIngredientMisses() {
            return ingredientMisses;
        }

        public void setIngredientMisses(List<Integer> ingredientMisses) {
            this.ingredientMisses = ingredientMisses;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }
}
