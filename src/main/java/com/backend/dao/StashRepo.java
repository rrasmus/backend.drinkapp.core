package com.backend.dao;

import com.backend.model.entity.account.Stash;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rasmus on 2016-06-11.
 */
@Repository
public interface StashRepo extends CrudRepository<Stash,Long>{
}
