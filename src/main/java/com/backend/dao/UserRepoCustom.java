package com.backend.dao;

import com.backend.model.entity.account.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by rasmus on 2016-06-03.
 */
@Repository
public interface UserRepoCustom extends JpaRepository<User,Long>{

    @Query("select u from User u where u.accessToken = ?1")
    User findByToken(String token);

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);

    @Query("select u from User u where u.swaggerKey = ?1")
    User findBySwaggerKey(String key);
}
