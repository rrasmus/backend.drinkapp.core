package com.backend.dao;

import com.backend.model.entity.api.IngredientImageData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by root on 6/26/16.
 */
@Repository
public interface IngredientImageDataRepo extends CrudRepository<IngredientImageData,Long> {
}
