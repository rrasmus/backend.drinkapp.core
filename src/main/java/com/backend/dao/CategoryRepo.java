package com.backend.dao;

import com.backend.model.entity.api.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rasmus on 2016-05-13.
 */
@Repository
public interface CategoryRepo extends CrudRepository<Category, Long> {
}
