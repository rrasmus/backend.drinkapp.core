package com.backend.dao;

import com.backend.model.entity.account.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rasmus on 2016-06-03.
 */
@Repository
public interface UserRepo extends CrudRepository<User,Long> {
}
