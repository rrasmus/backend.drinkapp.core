package com.backend.dao;

import com.backend.model.entity.api.Vendor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rasmus on 2016-05-13.
 */
@Repository
public interface VendorRepo  extends CrudRepository<Vendor, Long> {
}
