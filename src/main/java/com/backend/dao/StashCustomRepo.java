package com.backend.dao;

import com.backend.model.entity.account.Stash;
import com.backend.model.entity.account.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Created by rasmus on 2016-06-11.
 */
@Repository
public interface StashCustomRepo extends JpaRepository<Stash,Long> {

    @Query("select s from Stash s where s.user = ?1")
    Set<Stash> getStashFromUser(User user);

    @Query("select s from Stash s where s.id = ?1 and s.user=?2")
    Stash getStashFromUserWithId(Long id,User user);
}
