package com.backend.dao;

import com.backend.model.entity.account.Stash;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Ingredient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created by rasmus on 2016-05-14.
 */
@Repository
public interface IngredientRepo extends CrudRepository<Ingredient,Long> {

}
