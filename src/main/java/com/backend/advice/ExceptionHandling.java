package com.backend.advice;

import com.backend.model.exception.CustomGenericException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by rasmus on 2016-06-10.
 */


/*
@ControllerAdvice
public class ExceptionHandling extends ResponseEntityExceptionHandler {
    
    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object exception(Exception ex) {
        return new CustomGenericException(CustomGenericException.DEBUG_ERR_CODE,ex.getLocalizedMessage());
    }
}
*/
