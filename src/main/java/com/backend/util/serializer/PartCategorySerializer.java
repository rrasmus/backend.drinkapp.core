package com.backend.util.serializer;

import com.backend.model.entity.api.PartCategory;
import com.backend.model.entity.api.PartIngredient;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Created by rasmus on 8/4/16.
 */
public class PartCategorySerializer extends StdSerializer<PartCategory> {

    public PartCategorySerializer() {
        this(null);
    }

    public PartCategorySerializer(Class<PartCategory> t) {
        super(t);
    }

    @Override
    public void serialize(PartCategory partCategory, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("createdAt",partCategory.getCreatedAt());
        jsonGenerator.writeNumberField("latestModified",partCategory.getLatestModified());
        jsonGenerator.writeStringField("unit",partCategory.getUnit().toString());
        jsonGenerator.writeNumberField("category",partCategory.getCategory().getId());
        jsonGenerator.writeNumberField("quantity",partCategory.getQuantity());
        jsonGenerator.writeNumberField("id",partCategory.getId());
        jsonGenerator.writeEndObject();
    }
}
