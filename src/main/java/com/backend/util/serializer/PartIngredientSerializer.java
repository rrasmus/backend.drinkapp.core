package com.backend.util.serializer;

import com.backend.model.entity.api.PartIngredient;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Created by rasmus on 8/4/16.
 */
public class PartIngredientSerializer extends StdSerializer<PartIngredient> {

    public PartIngredientSerializer() {
        this(null);
    }

    public PartIngredientSerializer(Class<PartIngredient> t) {
        super(t);
    }

    @Override
    public void serialize(PartIngredient partIngredient, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("createdAt",partIngredient.getCreatedAt());
        jsonGenerator.writeNumberField("latestModified",partIngredient.getLatestModified());
        jsonGenerator.writeStringField("unit",partIngredient.getUnit().toString());
        jsonGenerator.writeNumberField("ingredient",partIngredient.getIngredient().getId());
        jsonGenerator.writeNumberField("quantity",partIngredient.getQuantity());
        jsonGenerator.writeNumberField("id",partIngredient.getId());
        jsonGenerator.writeEndObject();
    }
}
