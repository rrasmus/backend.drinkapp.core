package com.backend.util;

/**
 * Created by rasmus on 8/4/16.
 */
public class View {

    public static class Summary extends OnlyFk{

    }

    public static class Full extends Summary {

    }

    public static class Never extends Full {

    }

    public static class OnlyFk {

    }
}
