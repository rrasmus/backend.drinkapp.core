package com.backend.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rasmus on 2016-06-06.
 */
public class Resources {

    public static final Map<String,Integer> maxIngredients = new HashMap<>();

    static {
        maxIngredients.put(Roles.MOBILE_USER,20);
        maxIngredients.put(Roles.AUTHED_USER,50);
        maxIngredients.put(Roles.ADMIN_USER,Integer.MAX_VALUE);
        maxIngredients.put(Roles.SERVER_USER,Integer.MAX_VALUE);
    }

    public static class SecurityResources {

        public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
        public static final String SWAGGER_PARAMETER_NAME = "api_key";
    }

    public static class User {

        public static final String MOBILE_USER_PASSWORD = "123";
    }

    public class Roles {

        public final static String MOBILE_USER = "ROLE_MOBILE_USER";
        public final static String AUTHED_USER = "ROLE_AUTHED_USER";
        public final static String ADMIN_USER = "ROLE_ADMIN_USER";
        public final static String SERVER_USER = "ROLE_SERVER_USER";
    }
    public enum Scope {
        MOBILE_USER,USER,SERVER,ADMIN
    }

    public enum Platforms {
        MOBILE, WEB, CUSTOM
    }
}
