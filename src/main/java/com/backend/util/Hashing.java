package com.backend.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by rasmus on 2016-05-31.
 */
public class Hashing {

    private SecureRandom random;
    private MessageDigest md;

    private static Hashing instance;

    private Hashing() {
        random = new SecureRandom();
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Hashing getInstance() {
        if (instance == null) {
            instance = new Hashing();
        }
        return instance;
    }

    public HashedDto hashIt(String password,String salt) {
        HashedDto hashedDto = null;
        try {
            md.update(password.getBytes());
            byte[] hashedBytePw = md.digest();
            String hashedPw = new String(hashedBytePw,"UTF-8");
            String hashedRes = salt + hashedPw + salt;
            hashedDto = new HashedDto(hashedPw,salt,password);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return hashedDto;
        }
    }

    public HashedDto hashIt(String password) {
        HashedDto hashedDto = null;
        try {
            String salt = new String(random.generateSeed(20),"UTF-8");
            hashedDto = hashIt(password,salt);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            return hashedDto;
        }
    }

    public HashedDto hashIt() {
        HashedDto hashedDto = null;
        try {
            String pw = new String(random.generateSeed(20),"UTF-8");
            hashedDto = hashIt(pw);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            return hashedDto;
        }
    }

    /*
    * Class used to pass around hashing data
     */
    public class HashedDto {
        private String hashedPassword;
        private String salt;
        private String password;

        public HashedDto(String hashedPassword,String salt,String password) {
            this.hashedPassword = hashedPassword;
            this.salt = salt;
            this.password = password;
        }

        public String getHashedPassword() {
            return hashedPassword;
        }

        public void setHashedPassword(String hashedPassword) {
            this.hashedPassword = hashedPassword;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
