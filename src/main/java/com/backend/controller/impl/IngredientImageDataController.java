package com.backend.controller.impl;

import com.backend.dao.IngredientImageDataRepo;
import com.backend.model.dto.Response;
import com.backend.model.entity.api.IngredientImageData;
import com.backend.service.IIngredientImageUploadService;
import com.backend.util.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by root on 6/26/16.eclipselink vs hibernate
 */
@RestController
@RequestMapping("/ingredient-image-data")
public class IngredientImageDataController {

    @Autowired
    IIngredientImageUploadService ingredientExtraService;

    @Autowired
    IngredientImageDataRepo ingredientImageDataRepo;

    @RequestMapping(value="/upload-label", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> uploadNewLabel(@RequestParam Long ingredient,@RequestParam MultipartFile label) {

        try {
            IngredientImageData iid = ingredientExtraService.uploadLabel(ingredient,label);
            Response newReponse = new Response();
            newReponse.setResultData(iid);
            newReponse.setMessage("success");
            return newReponse.getData();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> getLabels() {

        try {
            Response newReponse = new Response();
            newReponse.setResultData(ingredientImageDataRepo.findAll());
            newReponse.setMessage("success");
            return newReponse.getData();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }
}
