package com.backend.controller.impl.crud;

import com.backend.controller.CrudRestController;
import com.backend.model.dto.Response;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Ingredient;
import com.backend.service.IIngredientImageUploadService;
import com.backend.util.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by rasmus on 2016-05-14.
 */
@RestController
@RequestMapping("/ingredient")
public class IngredientController extends CrudRestController<Ingredient,Long> {

    @Autowired
    IIngredientImageUploadService ingredientExtraService;

    @RequestMapping(value="/template-src", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> uploadNewTemplateSrc(@RequestParam Long ingredient, @RequestParam MultipartFile label) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        Ingredient ingredient1 = ingredientExtraService.uploadTemplateSrc(ingredient,label);
        Response resp = new Response();
        resp.setResultData(ingredient1);
        return resp.getData();
    }
}
