package com.backend.controller.impl.crud;

import com.backend.controller.CrudRestController;
import com.backend.model.dto.Response;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Recipe;
import com.backend.service.IRecipeService;
import com.backend.service.impl.RecipeService;
import com.backend.util.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by rasmus on 2016-05-14.
 */
@RestController
@RequestMapping("/recipe")
public class RecipeController extends CrudRestController<Recipe,Long> {

    @Override
    @PreAuthorize("hasRole('" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(method = RequestMethod.POST)
    public Map<String,Object> create(@RequestBody Recipe entity) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        entity.setAuthor(user);

        return super.create(entity);
    }
}
