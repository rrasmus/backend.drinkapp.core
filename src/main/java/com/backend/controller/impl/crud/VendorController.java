package com.backend.controller.impl.crud;

import com.backend.controller.CrudRestController;
import com.backend.model.entity.api.Vendor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rasmus on 2016-05-13.
 */
@RestController
@RequestMapping("/vendor")
public class VendorController extends CrudRestController<Vendor,Long> {
}
