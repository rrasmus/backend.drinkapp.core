package com.backend.controller.impl;

import com.backend.model.dto.CreateAccountDto;
import com.backend.model.dto.LoginAccountDto;
import com.backend.model.dto.Response;
import com.backend.model.entity.account.User;
import com.backend.service.IManagement;
import com.backend.util.Resources;
import com.backend.util.Tuple2;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rasmus on 2016-06-03.
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    IManagement management;

    @RequestMapping(value = "/create/mobile", method = RequestMethod.POST)
    public Map<String,Object> createMobileUser() {
        Tuple2<User,Object> data = management.createNewMobileUser();

        Map<String,Object>  fineFormatting = new HashMap<String,Object>();
        fineFormatting.put("account",data._0);
        fineFormatting.put("password",data._1);

        Response newReponse = new Response();
        newReponse.setResultData(fineFormatting);
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    //@PreAuthorize("hasRole('" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(value = "/create/admin", method = RequestMethod.POST)
    public Map<String,Object> createAdmin(@RequestBody @Valid CreateAccountDto newUser) {
        User newAdminUser = management.createNewAdmin(newUser.getUsername(),newUser.getPassword(),newUser.getEmail());
        Map<String,Object>  fineFormatting = new HashMap<String,Object>();
        newAdminUser.setPassword(null);
        fineFormatting.put("account",newAdminUser);

        Response newReponse = new Response();
        newReponse.setResultData(fineFormatting);
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public Map<String,Object> getMe() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Response newReponse = new Response();
        newReponse.setResultData(auth.getPrincipal());
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Map<String,Object> login(@RequestBody @Valid LoginAccountDto user) {
        Tuple2<User,String> returnVal = management.login(user.getUsername(),user.getPassword());

        if (returnVal._1 != null) {
            throw new RuntimeException(returnVal._1);
        }

        else {
            Map<String,String>  fineFormatting = new HashMap<String,String>();
            fineFormatting.put("access_token",returnVal._0.getAccToken());
            fineFormatting.put("swagger_token",returnVal._0.getSwaggerKey());

            Response newReponse = new Response();
            newReponse.setResultData(fineFormatting);
            newReponse.setMessage("success");
            return newReponse.getData();
        }
    }
}
