package com.backend.controller.impl;

import com.amazonaws.auth.AWSCredentials;
import com.backend.dao.StashCustomRepo;
import com.backend.dao.StashRepo;
import com.backend.model.dto.Response;
import com.backend.model.dto.StashCreateDto;
import com.backend.model.entity.account.Stash;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Ingredient;
import com.backend.util.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.*;

/**
 * Created by rasmus on 2016-06-11.
 */
@RestController
@RequestMapping("/stash")
public class StashController {

    @Autowired
    EntityManager em;

    @Autowired
    private StashCustomRepo stashCustomRepo;

    @Autowired
    private StashRepo stashRepo;

    @Autowired
    private AWSCredentials awsCredentials;

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> createStash(@RequestBody StashCreateDto stash) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        Stash newStash = new Stash();
        newStash.setUser(authedUser);
        newStash.setName(stash.getStashName());

        if (stash.getIngredientIds() != null) {
            newStash.setIngredients(new ArrayList<>());
            for (Long id : stash.getIngredientIds()) {
                Ingredient i = new Ingredient();
                i.setId(id);
                newStash.getIngredients().add(i);
            }
        }

        Stash savedStash = stashRepo.save(newStash);

        Response newReponse = new Response();
        newReponse.setResultData(savedStash);
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @RequestMapping(value="/{id}/ingredient", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> addToStash(@PathVariable(value="id") Long id, @RequestBody List<Long> ingredientIds) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        Stash stash = stashCustomRepo.getStashFromUserWithId(id,authedUser);

        if (stash == null) {
            throw new Exception("No stash with that id connected to the user");
        }

        if (ingredientIds != null) {
            for (Long ingredientId : ingredientIds) {
                boolean contains = false;

                for (Ingredient stashIngredient : stash.getIngredients()) {
                    if (stashIngredient.getId() == ingredientId) {
                        contains = true;
                    }
                }
                if (!contains) {
                    Ingredient i = new Ingredient();
                    i.setId(ingredientId);
                    stash.getIngredients().add(i);
                }
            }
        }

        Stash savedStash = stashRepo.save(stash);

        Response newReponse = new Response();
        newReponse.setMessage("success");
        newReponse.setResultData(savedStash);
        return newReponse.getData();
    }

    @RequestMapping(value="/{id}/ingredient", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> deleteFromStash(@PathVariable(value="id") Long id, @RequestBody List<Long> ingredientIds) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        Stash stash = stashCustomRepo.getStashFromUserWithId(id,authedUser);

        if (stash == null) {
            throw new Exception("No stash with that id connected to the user");
        }

        if (ingredientIds != null) {
            for (Long ingredientId : ingredientIds) {
                Ingredient i = null;

                for (Ingredient stashIngredient : stash.getIngredients()) {
                    if (stashIngredient.getId() == ingredientId) {
                        i = stashIngredient;
                    }
                }

                if (i != null) {
                    stash.getIngredients().remove(i);
                }
            }
        }

        Stash savedStash = stashRepo.save(stash);

        Response newReponse = new Response();
        newReponse.setMessage("success");
        newReponse.setResultData(savedStash);
        return newReponse.getData();
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> getStash() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        EntityGraph eg = em.getEntityGraph("Stash.findNoInclude");
        List stashes = em.createNamedQuery("Stash.findAll",Stash.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .setParameter("user",authedUser)
                .getResultList();

        Response newReponse = new Response();
        newReponse.setResultData(stashes);
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> getStashWithId(@PathVariable(value="id") Long id) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        EntityGraph eg = em.getEntityGraph("Stash.find");
        Stash stash = em.createNamedQuery("Stash.findId",Stash.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .setParameter("user",authedUser)
                .setParameter("id",id)
                .getSingleResult();

        if (stash == null) {
            throw new Exception("There is no stash connected to that user with that id");
        }

        Response newReponse = new Response();
        newReponse.setResultData(stash);
        newReponse.setMessage("success");
        return newReponse.getData();
    }


    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole('" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.ADMIN_USER + "')")
    public Map<String,Object> updateStash(@PathVariable(value="id") Long id, @RequestBody StashCreateDto stashDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authedUser =  (User) auth.getPrincipal();

        Stash stash = stashCustomRepo.getStashFromUserWithId(id,authedUser);

        if (stashDto.getStashName() != null) {
            stash.setName(stashDto.getStashName());
        }


        /*
        if (file != null) {

            AmazonS3Client s3Client = new AmazonS3Client(awsCredentials);
            String fileEnding = file.getContentType().split("/")[1];
            String filename = stash.getUrl();

            if (filename.equals("")) {
                filename = UUID.randomUUID().toString() + "." + fileEnding;
                stash.setUrl(filename);
            }
            try {
                s3Client.putObject(new PutObjectRequest(AwsResources.IMAGE_BUCKET, filename, file.getInputStream(), new ObjectMetadata()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        */


        Stash savedStash = stashRepo.save(stash);

        Response newReponse = new Response();
        newReponse.setResultData(savedStash);
        newReponse.setMessage("success");
        return newReponse.getData();
    }
}
