package com.backend.controller.impl;

import com.backend.dao.SearchRepo;
import com.backend.model.dto.Response;
import com.backend.model.dto.SearchDto;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Recipe;
import com.backend.service.ISearchService;
import com.backend.util.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * Created by rasmus on 6/15/16.
 */
@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    SearchRepo searchRepo;

    @Autowired
    ISearchService iSearchService;

    public static int MAX_CATEGORY_LIST_SIZE = 500;


    @PreAuthorize("hasAnyRole('" + Resources.Roles.ADMIN_USER + "','" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.AUTHED_USER + "')")
    @RequestMapping(method = RequestMethod.GET)
    public Map<String,Object> getRecipes(@RequestParam List<Integer> ingredientIds, @RequestParam(required = false) List<Integer> categoryIncludes, SearchRepo.OrderSearchBy orderSearchBy) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        // check list sizes
        int maxNumberOfSearchIngredients = user.getMaxSearchIngreds();

        if (ingredientIds.size() > maxNumberOfSearchIngredients ) {
            throw new Exception("Too many ingredients for this user");
        }

        if (categoryIncludes != null && categoryIncludes.size() > MAX_CATEGORY_LIST_SIZE) {
            throw new Exception("Too many categories in the list hard capped at " + MAX_CATEGORY_LIST_SIZE);
        }

        Response newReponse = new Response();
        newReponse.setResultData(searchRepo.getRecipes(ingredientIds,categoryIncludes,orderSearchBy));
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @PreAuthorize("hasAnyRole('" + Resources.Roles.ADMIN_USER + "','" + Resources.Roles.MOBILE_USER + "','" + Resources.Roles.AUTHED_USER + "','" + Resources.Roles.AUTHED_USER + "')")
    @RequestMapping(value = "/photo", method = RequestMethod.POST)
    public Map<String,Object> getPhoto(@RequestParam MultipartFile label) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        long res  = iSearchService.search(user,label);
        if (res == ISearchService.ERROR_TICKET) {
            throw new Exception("Could not set message");
        }

        Response newReponse = new Response();
        newReponse.setResultData(res);
        return newReponse.getData();
    }
}
