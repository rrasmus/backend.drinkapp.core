package com.backend.controller;

import com.backend.model.dto.Response;
import com.backend.model.entity.api.Ingredient;
import com.backend.model.entity.api.Recipe;
import com.backend.model.util.IMapper;
import com.backend.model.util.Mapper;
import com.backend.service.CrudService;
import com.backend.util.Resources;
import com.backend.util.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by rasmus on 2016-05-13.
 */
public abstract class CrudRestController<ENTITY,ID extends Serializable> {

    @Autowired
    CrudRepository<ENTITY,ID> repo;

    @Autowired
    CrudService<ENTITY,ID> crudService;

    @PreAuthorize("hasRole('" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(method = RequestMethod.POST)
    public Map<String,Object> create(@RequestBody ENTITY entity) {
        Response newReponse = new Response();
        newReponse.setResultData(crudService.create(entity));
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @RequestMapping(method = RequestMethod.GET)
    public Map<String,Object> findAll(){
        Response newReponse = new Response();
        newReponse.setResultData(crudService.findAll());
        newReponse.setMessage("success");

        return newReponse.getData();
    }

    @PreAuthorize("hasRole('" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Map<String,Object> delete(@PathVariable("id") ID id) {
        repo.delete(id);
        Response newReponse = new Response();
        newReponse.setResultData("DOTA > LOL");
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Map<String,Object> get(@PathVariable("id") ID id) {
        Response newReponse = new Response();
        newReponse.setResultData(crudService.findOne(id));
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    // same as create but for clarity
    @PreAuthorize("hasRole('" + Resources.Roles.ADMIN_USER + "')")
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public Map<String,Object> update(@PathVariable("id") ID id,@RequestBody ENTITY entity) throws Exception {
        IMapper<ENTITY,ENTITY> mapper = Mapper.getInstance().getMapper(entity.getClass(),entity.getClass());

        if (mapper == null) {
            throw new Exception("Internal error, could not find mapper for entity");
        }

        ENTITY entityInDb = repo.findOne(id);
        mapper.copyProperties(entity,entityInDb);
        ENTITY updated = repo.save(entityInDb);

        Response newReponse = new Response();
        newReponse.setResultData(updated);
        newReponse.setMessage("success");
        return newReponse.getData();
    }

    protected CrudRepository<ENTITY,ID> getRepo() {
        return repo;
    }

    protected CrudService<ENTITY,ID> getCrudService() {
        return crudService;
    }
}
