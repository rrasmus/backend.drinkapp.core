package com.backend.service;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.backend.config.aws.AwsResources;
import com.backend.model.entity.account.User;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

/**
 * Created by rasmus on 12/26/16.
 */
public class S3Helper {

    public static String checkAndCreateUserMap(User user, AmazonS3Client amazonS3Client,String bucket) {
        String userFolderPath = user.getId().toString();
        try {
            ObjectMetadata omd = amazonS3Client.getObjectMetadata(bucket, userFolderPath);
        } catch (AmazonS3Exception exp) {
            try {
                InputStream stream = new ByteArrayInputStream(Calendar.getInstance().getTime().toString().getBytes(StandardCharsets.UTF_8));
                amazonS3Client.putObject(bucket,userFolderPath + "/create",stream,new ObjectMetadata());
            } catch(Exception e) {
                return null;
            }
        }

        return userFolderPath;
    }

    public static String checkAndCreateUserPhotoMap(User user, AmazonS3Client amazonS3Client, String bucket) {
        String userMapPath = checkAndCreateUserMap(user,amazonS3Client,bucket);
        if (userMapPath == null) {
            return null;
        }
        String photoMapPath = userMapPath + "/" + AwsResources.S3.USER_PHOTO_MAP_NAME;

        try {
            ObjectMetadata omd = amazonS3Client.getObjectMetadata(bucket, photoMapPath);
        } catch (AmazonS3Exception exp) {
            try {
                InputStream stream = new ByteArrayInputStream(Calendar.getInstance().getTime().toString().getBytes(StandardCharsets.UTF_8));
                amazonS3Client.putObject(bucket,photoMapPath + "/create",stream,new ObjectMetadata());
            } catch(Exception e) {
                return null;
            }
        }
        return photoMapPath;
    }
}
