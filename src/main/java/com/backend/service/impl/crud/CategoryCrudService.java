package com.backend.service.impl.crud;

import com.backend.model.entity.api.Category;
import com.backend.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by rasmus on 8/4/16.
 */
@Service
@Transactional
public class CategoryCrudService extends CrudService<Category,Long> {

    @Autowired
    EntityManager em;

    @Override
    public Category findOne(Long id) {
        Category category = getRepo().findOne(id);

        if (category != null) {
            category.getIngredientSet().size();
        }
        return category;
    }

    @Override
    public Iterable<Category> findAll() {
        EntityGraph eg = em.getEntityGraph("Category.ingredient");
        List cats = em.createNamedQuery("Category.findAll",Category.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
        return cats;
    }
}
