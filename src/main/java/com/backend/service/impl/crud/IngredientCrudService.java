package com.backend.service.impl.crud;

import com.backend.model.entity.api.Ingredient;
import com.backend.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by rasmus on 8/4/16.
 */
@Service
@Transactional
public class IngredientCrudService extends CrudService<Ingredient,Long> {


    @Autowired
    EntityManager em;

    @Override
    public Ingredient findOne(Long id) {
        Ingredient ingredient = getRepo().findOne(id);

        if (ingredient != null) {
            ingredient.getCategorySet().size();
        }
        return ingredient;
    }

    @Override
    public Iterable<Ingredient> findAll() {
        EntityGraph eg = em.getEntityGraph("Ingredient.category");
        List ingreds = em.createNamedQuery("Ingredient.findAll",Ingredient.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
        return ingreds;
    }
}
