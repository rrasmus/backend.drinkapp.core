package com.backend.service.impl.crud;

import com.backend.model.entity.api.Recipe;
import com.backend.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;

/**
 * Created by rasmus on 8/4/16.
 */
@Service
@Transactional
public class RecipeCrudService extends CrudService<Recipe,Long> {


    @Autowired
    EntityManager em;

    @Override
    public Recipe create(Recipe entity) {
        Recipe recipe =  getRepo().save(entity);
        return recipe;
    }

    @Override
    public Recipe findOne(Long id) {
        EntityGraph eg = em.getEntityGraph("Recipe.find");
        Recipe recipe = em.createNamedQuery("Recipe.findOne",Recipe.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .setParameter("id",id)
                .getSingleResult();
        return recipe;
    }
}
