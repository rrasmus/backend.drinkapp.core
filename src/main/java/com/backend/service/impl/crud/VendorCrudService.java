package com.backend.service.impl.crud;

import com.backend.model.entity.api.Vendor;
import com.backend.service.CrudService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rasmus on 8/4/16.
 */
@Service
@Transactional
public class VendorCrudService extends CrudService<Vendor,Long> {
}
