package com.backend.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.backend.config.aws.AwsResources;
import com.backend.config.aws.sqs.PhotoMessageSqs;
import com.backend.dao.PhotoRepo;
import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Photo;
import com.backend.model.entity.api.Recipe;
import com.backend.service.ISearchService;
import com.backend.service.S3Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


/**
 * Created by rasmus on 6/15/16.
 */
@Service
public class SearchService implements ISearchService {

    @Autowired
    private PhotoRepo photoRepo;

    @Autowired
    private AWSCredentials awsCredentials;

    private AmazonS3Client s3Client;

    private AmazonSQSClient amazonSQSClient;

    @PostConstruct
    public void initiateAws() {
        s3Client = new AmazonS3Client(awsCredentials);
        s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));
        amazonSQSClient = new AmazonSQSClient(awsCredentials);
        amazonSQSClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
    }
    @Override
    public List<Recipe> search(List<Integer> ingredientsIds) {
        return null;
    }

    @Transactional
    public long search(User user, MultipartFile label) {
        String fileEnding = label.getContentType().split("/")[1];
        String filename = UUID.randomUUID().toString() + "." + fileEnding;

        String imageMapPath = S3Helper.checkAndCreateUserPhotoMap(user,s3Client,AwsResources.S3.BUCKET_NAME_USER);
        try {
            s3Client.putObject(new PutObjectRequest(AwsResources.S3.BUCKET_NAME_USER,imageMapPath + "/" + filename,label.getInputStream(), new ObjectMetadata()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Photo photo = new Photo();
        photo.setLink(filename);
        photo.setUser(user);
        photo = photoRepo.save(photo);


        PhotoMessageSqs photoMessageSqs = new PhotoMessageSqs();
        photoMessageSqs.setLink(user.getId() + "/photos/" + photo.getLink());
        photoMessageSqs.setPhotoId(photo.getId());
        amazonSQSClient.sendMessage(AwsResources.SQS.QUEUE_URL,photoMessageSqs.getJsonObject().toString());

        return photo.getId();
    }
}
