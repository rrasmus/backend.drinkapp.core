package com.backend.service.impl;

import com.backend.dao.RecipeRepo;
import com.backend.model.dto.PartDto;
import com.backend.model.entity.api.*;
import com.backend.service.IRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rasmus on 2016-06-04.
 */
@Service
@Transactional
public class RecipeService implements IRecipeService {

    @Autowired
    EntityManager em;

    @Autowired
    private RecipeRepo recipeRepo;


    @Override
    public List<Recipe> findAll() {
        EntityGraph eg = em.getEntityGraph("Recipe.find");
        List recipies = em.createNamedQuery("Recipe.findAll",Recipe.class)
                .setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
        return recipies;
    }

    @Override
    public Recipe create(String title, String body, Set<PartDto> partCategories, Set<PartDto> partIngredients) {
        Recipe recipe = new Recipe();
        recipe.setName(title);
        recipe.setBody(body);

        recipe.setPartsCats(new HashSet<>());
        recipe.setPartsIngreds(new HashSet<>());
        return populateAndSaveRecipe(recipe,title,body,partCategories,partIngredients);
    }

    @Override
    public Recipe update(Long id, String title,String body, Set<PartDto> partCategories, Set<PartDto> partIngredients) {
        Recipe savedRecipe = recipeRepo.findOne(id);
        return populateAndSaveRecipe(savedRecipe,title,body,partCategories,partIngredients);
    }

    private Recipe populateAndSaveRecipe(Recipe recipe,String title, String body, Set<PartDto> partCategories, Set<PartDto> partIngredients) {
        //Recipe recipe = new Recipe();
        //recipe.setTitle(title);
        //recipe.setBody(body);

        //recipe.setPartsCats(new ArrayList<>());
        //recipe.setPartsIngreds(new ArrayList<>());

        // TODO: change this to a mapper shit instead
        if (partCategories != null) {

            for (PartDto dto : partCategories) {
                PartCategory newPartCategory = new PartCategory();
                newPartCategory.setQuantity(dto.quantity);

                Category newCategory = new Category();
                newCategory.setId(dto.id);

                newPartCategory.setUnit(dto.unitId);
                newPartCategory.setCategory(newCategory);

                recipe.getPartsCats().add(newPartCategory);
            }
        }

        if (partIngredients != null) {

            for (PartDto dto : partIngredients) {
                PartIngredient newPartIngredeint = new PartIngredient();
                newPartIngredeint.setQuantity(dto.quantity);

                Ingredient newIngredient = new Ingredient();
                newIngredient.setId(dto.id);

                newPartIngredeint.setUnit(dto.unitId);
                newPartIngredeint.setIngredient(newIngredient);

                recipe.getPartsIngreds().add(newPartIngredeint);
            }
        }

        Recipe savedRecipe = recipeRepo.save(recipe);

        return savedRecipe;
    }
}
