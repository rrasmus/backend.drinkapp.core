package com.backend.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.backend.config.aws.AwsResources;
import com.backend.config.aws.sqs.TemplateMessageSqs;
import com.backend.dao.IngredientImageDataRepo;
import com.backend.dao.IngredientRepo;
import com.backend.model.entity.api.Ingredient;
import com.backend.model.entity.api.IngredientImageData;
import com.backend.service.IIngredientImageUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by root on 6/26/16.
 */
@Service
public class IngredientImageUploadService implements IIngredientImageUploadService {

    @Autowired
    IngredientRepo ingredientRepo;

    @Autowired
    IngredientImageDataRepo imageDataRepo;

    @Autowired
    private AWSCredentials awsCredentials;

    private AmazonSQSClient amazonSQSClient;

    private AmazonS3Client s3Client;

    @PostConstruct
    public void initiateAws() {
        s3Client = new AmazonS3Client(awsCredentials);
        s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));
        amazonSQSClient = new AmazonSQSClient(awsCredentials);
        amazonSQSClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
    }

    public Ingredient uploadTemplateSrc(Long ingredient, MultipartFile label) throws Exception {
        Ingredient findOne = ingredientRepo.findOne(ingredient);

        if (findOne == null) {
            // TODO: custom exception
            throw new Exception("Todo");
        }
        boolean isNew = findOne.getTemplateSrc() == null;

        String fileEnding = label.getContentType().split("/")[1];
        String filename;
        if (isNew) {
            filename = UUID.randomUUID().toString() + "." + fileEnding;
        } else {
            filename = findOne.getTemplateSrc();
        }
        try {
            s3Client.putObject(new PutObjectRequest(AwsResources.S3.BUCKET_NAME_TEMPLATE,filename,label.getInputStream(), new ObjectMetadata()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Send the template to be processed by some vision server
        TemplateMessageSqs templateMessageSqs = new TemplateMessageSqs();
        if (isNew) {
            templateMessageSqs.setMethod(TemplateMessageSqs.METHOD_NEW);
        } else {
            templateMessageSqs.setMethod(TemplateMessageSqs.METHOD_UPDATE);
        }
        templateMessageSqs.setLink(filename);
        templateMessageSqs.setId(findOne.getId());
        amazonSQSClient.sendMessage(AwsResources.SQS.QUEUE_URL,templateMessageSqs.getJsonObject().toString());

        return findOne;
    }

    @Override
    public IngredientImageData uploadLabel(Long ingredient, MultipartFile label) throws Exception {
        return null;
        /*
        Ingredient findOne = ingredientRepo.findOne(ingredient);

        if (findOne == null) {
            // TODO: custom exception
            throw new Exception("Todo");
        }

        // Write to s3 bucket
        String fileEnding = label.getContentType().split("/")[1];
        String filename = UUID.randomUUID().toString() + "." + fileEnding;
        try {
            s3Client.putObject(new PutObjectRequest(AwsResources.S3.BUCKET_NAME_TEMPLATE,filename,label.getInputStream(), new ObjectMetadata()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        IngredientImageData newIID = new IngredientImageData();
        newIID.setIngredient(findOne);
        newIID.setImageUrl(filename);
        imageDataRepo.save(newIID);

        return newIID;
        */
    }
}
