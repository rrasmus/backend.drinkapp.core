package com.backend.service.impl;

import com.backend.dao.UserRepo;
import java.util.UUID;

import com.backend.dao.UserRepoCustom;
import com.backend.model.entity.account.User;
import com.backend.service.IManagement;
import com.backend.util.Resources;
import com.backend.util.Tuple2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by rasmus on 2016-05-31.
 */
@Service
@Transactional
public class UserManagementService implements IManagement {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserRepoCustom userRepoCustom;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    /*
    * STATICS
     */
    private static final String COULD_NOT_LOGIN = "Could not login, check username or password";

    @Override
    public Tuple2<User,Object> createNewMobileUser() {
        String pwForNow = Resources.User.MOBILE_USER_PASSWORD;

        User newMobileUser = new User();
        newMobileUser.setCreatedByPlatform(Resources.Platforms.MOBILE);
        newMobileUser.setAutoCreated(true);
        newMobileUser.setPassword(encoder.encode(pwForNow));
        newMobileUser.setUsername(UUID.randomUUID().toString());
        newMobileUser.setEnabled(true);
        newMobileUser.setMaxSearchIngreds(Resources.maxIngredients.get(Resources.Roles.MOBILE_USER));
        newMobileUser.setRole(Resources.Roles.MOBILE_USER);
        newMobileUser.setAccToken(UUID.randomUUID().toString());
        newMobileUser.setSwaggerKey(UUID.randomUUID().toString());
        User savedUser = userRepo.save(newMobileUser);

        return new Tuple2(savedUser,pwForNow);
    }

    @Override
    public User createNewAdmin(String username, String password, String email) {
        User newAdminUser = new User();
        newAdminUser.setCreatedByPlatform(Resources.Platforms.WEB);
        newAdminUser.setPassword(encoder.encode(password));
        newAdminUser.setUsername(username);
        newAdminUser.setEnabled(true);
        newAdminUser.setMaxSearchIngreds(Resources.maxIngredients.get(Resources.Roles.ADMIN_USER));
        newAdminUser.setRole(Resources.Roles.ADMIN_USER);
        newAdminUser.setAccToken(UUID.randomUUID().toString());
        newAdminUser.setSwaggerKey(UUID.randomUUID().toString());

        User savedUser = userRepo.save(newAdminUser);
        return savedUser;
    }


    @Override
    public Tuple2<User,String> login(String username, String password) {
        User user = userRepoCustom.findByUsername(username);

        if (user == null) {
            return new Tuple2<User,String>(null, COULD_NOT_LOGIN);
        }

        if (encoder.matches(password,user.getPassword())) {
            //user.setAccToken(UUID.randomUUID().toString());
            //user = userRepo.save(user);
            return new Tuple2<User,String>(user,null);
        }

        return new Tuple2<User,String>(null, COULD_NOT_LOGIN);
    }
}
