package com.backend.service;

import com.backend.model.entity.api.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

/**
 * Created by rasmus on 8/4/16.
 */
public abstract class CrudService<ENTITY,ID extends Serializable> {

    @Autowired
    CrudRepository<ENTITY,ID> repo;

    public ENTITY findOne(ID id) {
        return repo.findOne(id);
    }

    public ENTITY create(ENTITY entity) {
        return repo.save(entity);
    }

    public Iterable<ENTITY> findAll() {
        return repo.findAll();
    }

    protected CrudRepository<ENTITY,ID> getRepo() {
        return repo;
    }
}
