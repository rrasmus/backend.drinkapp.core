package com.backend.service;

import com.backend.model.entity.account.User;
import com.backend.model.entity.api.Recipe;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by rasmus on 6/15/16.
 */
public interface ISearchService {

    public long ERROR_TICKET = -1L;

    public List<Recipe> search(List<Integer> ingredientsIds);
    public long search(User user, MultipartFile label);
}
