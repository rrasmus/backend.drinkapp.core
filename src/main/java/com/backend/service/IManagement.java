package com.backend.service;

import com.backend.model.entity.account.User;
import com.backend.util.Tuple2;

/**
 * Created by rasmus on 2016-05-31.
 */
public interface IManagement {

    Tuple2<User,Object> createNewMobileUser();
    User createNewAdmin(String username,String password,String email);
    Tuple2<User,String> login(String username,String password);
}
