package com.backend.service;

import com.backend.model.dto.PartDto;
import com.backend.model.entity.api.Recipe;

import java.util.List;
import java.util.Set;

/**
 * Created by rasmus on 2016-06-04.
 */
public interface IRecipeService {

    Recipe create(String title, String body, Set<PartDto> partCategories, Set<PartDto> partIngredients);

    Recipe update(Long id,String title, String body, Set<PartDto> partCategories, Set<PartDto> partIngredients);

    List<Recipe> findAll();
}
