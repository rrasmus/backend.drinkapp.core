package com.backend.service;

import com.backend.model.entity.api.Ingredient;
import com.backend.model.entity.api.IngredientImageData;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by root on 6/26/16.
 */
public interface IIngredientImageUploadService {

    IngredientImageData uploadLabel(Long ingredient, MultipartFile label) throws Exception;
    Ingredient uploadTemplateSrc(Long ingredient, MultipartFile label) throws Exception;
}
