package com.backend.model.util;

/**
 * Created by rasmus on 2016-06-04.
 */
public class View {
    public interface Anno {}
    public interface User extends Anno{}
    public interface UserImproved extends User {}
    public interface Admin extends UserImproved {}
    public interface FullMongo extends Admin {}
    public interface FullMongo2xRetard extends FullMongo {}
}