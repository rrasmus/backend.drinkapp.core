package com.backend.model.util;

/**
 * Created by rasmus on 8/4/16.
 */
public enum Unit {
    ML,DL,CL,L,ST,KRM,NAVE,TEDSKED,MADSKED
}
