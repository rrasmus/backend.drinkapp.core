package com.backend.model.util;

/**
 * Created by rasmus on 2016-05-13.
 */
public interface IMapper<F,T> {

    T mapFrom(F f);
    void copyProperties(F from,T dest);
    Class<T> getToClass();
    Class<F> getFromClass();

}
