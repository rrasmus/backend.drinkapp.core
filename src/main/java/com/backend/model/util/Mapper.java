package com.backend.model.util;

import com.backend.model.entity.api.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-13.
 * Singleton class
 */
public class Mapper {

    private static Mapper instance = null;
    private Map<String,Map<String,IMapper>> registry;

    private Mapper() {
        setRegistry(new HashMap<>());
        initMapper();
    }

    // unreachable intiation outside of this class

    private void setRegistry( Map<String,Map<String,IMapper>> reg) {
        this.registry = reg;
    }

    // for now
    private void initMapper() {
        class VendorMapper implements IMapper<Vendor,Vendor>  {

            @Override
            public Vendor mapFrom(Vendor vendor) {
                return null;
            }

            @Override
            public void copyProperties(Vendor from, Vendor dest) {
                if (from.getName() != null) {
                    dest.setName(from.getName());
                }
            }

            @Override
            public Class<Vendor> getToClass() {
                return Vendor.class;
            }

            @Override
            public Class<Vendor> getFromClass() {
                return Vendor.class;
            }
        }

        class CategoryMapper implements IMapper<Category,Category>  {

            @Override
            public Category mapFrom(Category vendor) {
                return null;
            }

            @Override
            public void copyProperties(Category from, Category dest) {
                if (from.getName() != null) {
                    dest.setName(from.getName());
                }
            }

            @Override
            public Class<Category> getToClass() {
                return Category.class;
            }

            @Override
            public Class<Category> getFromClass() {
                return Category.class;
            }
        }

        class IngredientMapper implements IMapper<Ingredient,Ingredient>  {

            @Override
            public Ingredient mapFrom(Ingredient vendor) {
                return null;
            }

            @Override
            public void copyProperties(Ingredient from, Ingredient dest) {
                if (from.getName() != null) {
                    dest.setName(from.getName());
                }

                dest.getCategorySet().clear();
                Set<Category> fromCategories = from.getCategorySet();

                if (fromCategories == null) {
                    return;
                }
                else{
                    dest.setCategorySet(fromCategories);
                }
            }

            @Override
            public Class<Ingredient> getToClass() {
                return Ingredient.class;
            }

            @Override
            public Class<Ingredient> getFromClass() {
                return Ingredient.class;
            }
        }

        class RecipeMapper implements IMapper<Recipe,Recipe> {

            @Override
            public Recipe mapFrom(Recipe recipe) {
                return null;
            }

            @Override
            public void copyProperties(Recipe from, Recipe dest) {
                if (from.getName() != null) {
                    dest.setName(from.getName());
                }

                if (from.getBody() != null) {
                    dest.setBody(from.getBody());
                }

                dest.getPartsCats().clear();
                if (from.getPartsCats() != null) {
                    dest.getPartsCats().addAll(from.getPartsCats());
                }

                dest.getPartsIngreds().clear();
                if (from.getPartsIngreds() != null) {
                    dest.getPartsIngreds().addAll(from.getPartsIngreds());
                }

                return;
            }

            @Override
            public Class<Recipe> getToClass() {
                return Recipe.class;
            }

            @Override
            public Class<Recipe> getFromClass() {
                return Recipe.class;
            }
        }

        this.addMapper(new VendorMapper());
        this.addMapper(new CategoryMapper());
        this.addMapper(new IngredientMapper());
        this.addMapper(new RecipeMapper());
    }

    //

    public static Mapper getInstance() {
        if(instance == null) {
            instance = new Mapper();
        }
        return instance;
    }

    public void addMapper(IMapper mapper) {
        String from = mapper.getFromClass().getName();
        String to = mapper.getToClass().getName();

        Map<String,IMapper> internalMap;
        if (registry.containsKey(from)) {
            internalMap = registry.get(from);
        } else {
            internalMap = new HashMap<>();
            registry.put(from,internalMap);
        }
        internalMap.put(to,mapper);
    }

    public IMapper getMapper(Class from, Class to) {
        if (registry.containsKey(from.getName())) {
            Map<String,IMapper> internalMap = registry.get(from.getName());
            return internalMap.get(to.getName());
        }
        return null;
    }
}
