package com.backend.model.exception;

/**
 * Created by rasmus on 2016-06-10.
 */
public class CustomGenericException {

    private int errCode;
    private String errMsg;

    public static final int DEBUG_ERR_CODE = 10;

    public CustomGenericException() {

    }

    public CustomGenericException(int errCode,String msg) {
        this.errCode = errCode;
        this.errMsg = msg;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
