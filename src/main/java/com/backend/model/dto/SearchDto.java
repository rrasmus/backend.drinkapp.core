package com.backend.model.dto;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rasmus on 6/15/16.
 */
public class SearchDto {

    private Integer recipeId;
    private Set<Integer> ingredientMatches;
    private Set<Integer> ingredientNoMatches;
    private Set<Integer> categoryNoMatches;
    private Set<Integer> categoryMatches;

    public SearchDto() {
        ingredientMatches = new HashSet<>();
        ingredientNoMatches = new HashSet<>();
        categoryNoMatches = new HashSet<>();
        categoryMatches = new HashSet<>();
    }

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public Set<Integer> getCategoryMatches() {
        return categoryMatches;
    }

    public void setCategoryMatches(Set<Integer> categoryMatches) {
        this.categoryMatches = categoryMatches;
    }

    public Set<Integer> getIngredientMatches() {
        return ingredientMatches;
    }

    public void setIngredientMatches(Set<Integer> ingredientMatches) {
        this.ingredientMatches = ingredientMatches;
    }

    public Set<Integer> getIngredientNoMatches() {
        return ingredientNoMatches;
    }


    public void setIngredientNoMatches(Set<Integer> ingredientNoMatches) {
        this.ingredientNoMatches = ingredientNoMatches;
    }

    public Set<Integer> getCategoryNoMatches() {
        return categoryNoMatches;
    }

    public void setCategoryNoMatches(Set<Integer> categoryNoMatches) {
        this.categoryNoMatches = categoryNoMatches;
    }

    public void addMissedCategory(int category) {
        if (!categoryMatches.contains(category)) {
            categoryNoMatches.add(category);
        }
    }

    public void addMatchIngred(int ingred) {
        ingredientNoMatches.remove(ingred);
        ingredientMatches.add(ingred);
    }

    public void addMatchIngredFromCategory(int category,int ingred) {
        categoryNoMatches.remove(category);
        categoryMatches.add(category);
        addMatchIngred(ingred);
    }

    public void addMissedIngred(int ingred) {
        if (!ingredientMatches.contains(ingred)) {
            ingredientNoMatches.add(ingred);
        }
    }
}
