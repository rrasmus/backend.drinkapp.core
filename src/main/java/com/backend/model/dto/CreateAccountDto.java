package com.backend.model.dto;

import org.hibernate.validator.constraints.Email;

/**
 * Created by rasmus on 8/4/16.
 */
public class CreateAccountDto extends LoginAccountDto {

    @Email
    private String email;

    public CreateAccountDto() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
