package com.backend.model.dto;

import com.backend.model.entity.account.Stash;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by rasmus on 2016-06-11.
 */
public class StashCreateDto {

    private String stashName;

    private List<Long> ingredientIds;


    public List<Long> getIngredientIds() {
        return ingredientIds;
    }

    public void setIngredientIds(List<Long> ingredientIds) {
        this.ingredientIds = ingredientIds;
    }

    public String getStashName() {
        return stashName;
    }

    public void setStashName(String stashName) {
        this.stashName = stashName;
    }
}
