package com.backend.model.dto;

import com.backend.model.util.Unit;

/**
 * Created by rasmus on 2016-06-04.
 */

public class PartDto {

    public Long id;
    public float quantity;
    public Unit unitId;


    public PartDto() {

    }


    public void setId(Long id) {
        this.id = id;
    }


    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }


    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }
}