package com.backend.model.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rasmus on 2016-05-13.
 */
public class Response {

    private Map<String,Object> data;

    private final static String RESULT_KEY = "result";
    private final static String MESSAGE_KEY = "message";
    private final static String DATA_KEY = "data";

    public Response() {
        this.data = new HashMap<>();
        this.data.put(RESULT_KEY,new HashMap<String,Object>());
    }

    public void setResultData(Object obj) {
        getResult().put(DATA_KEY,obj);
    }

    public void setMessage(String message) {
        data.put(MESSAGE_KEY,message);
    }

    public Map<String,Object> getMap() {
        return data;
    }

    public Map<String,Object> getResult() {
        return (HashMap) data.get(RESULT_KEY);
    }

    public Map<String,Object> getData() {
        return data;
    }
}
