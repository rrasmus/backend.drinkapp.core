package com.backend.model.dto;

import java.util.Set;

/**
 * Created by rasmus on 2016-06-04.
 */
public class CreateDto {
    public String title;
    public String body;
    public Set<PartDto> partCategories;
    public Set<PartDto> partIngredients;

    public CreateDto() {

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setPartCategories(Set<PartDto> partCategories) {
        this.partCategories = partCategories;
    }

    public void setPartIngredients(Set<PartDto> partIngredients) {
        this.partIngredients = partIngredients;
    }
}