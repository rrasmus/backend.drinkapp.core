package com.backend.model.dto;

import javax.validation.constraints.Size;

/**
 * Created by rasmus on 8/4/16.
 */
public class LoginAccountDto {

    @Size(min = 1, max = 64)
    private String username;
    @Size(min = 4)
    private String password;

    public LoginAccountDto() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
