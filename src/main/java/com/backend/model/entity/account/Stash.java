package com.backend.model.entity.account;

import com.backend.model.entity.BaseEntity;
import com.backend.model.entity.api.Ingredient;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rasmus on 2016-06-01.
 */
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"user" , "name"})})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Stash.find", attributeNodes = {
                @NamedAttributeNode(value = "ingredients")
        }),
        @NamedEntityGraph(name = "Stash.findNoInclude")
})
@NamedQueries({
        @NamedQuery(name = "Stash.findAll", query = "select distinct s from Stash s where s.user = :user"),
        @NamedQuery(name = "Stash.findId", query = "select distinct s from Stash s where s.user = :user and s.id = :id")
})
public class Stash extends BaseEntity {

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "user", referencedColumnName = "username")
    private User user;

    @ManyToMany
    private List<Ingredient> ingredients;

    @Column(name = "url")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Ingredient> getIngredients() {
        if (ingredients == null) {
            return new ArrayList<>();
        }
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
