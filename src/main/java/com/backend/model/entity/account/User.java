package com.backend.model.entity.account;

import com.backend.model.entity.BaseEntity;
import com.backend.model.entity.BaseTime;
import com.backend.model.entity.api.Photo;
import com.backend.model.entity.api.Recipe;
import com.backend.util.Resources;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-31.
 */
@Entity
public class User extends BaseEntity {

    @Email
    @Column(unique = true,name = "email")
    private String email;

    @Column(name = "phone_id")
    private String phoneId;

    @NotNull
    @Size(min = 4)
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "author")
    private Set<Recipe> recipes;

    @OneToMany(mappedBy = "user")
    private Set<Photo> photos;

    @Column(name = "max_search_ingreds")
    private int maxSearchIngreds = Resources.maxIngredients.get(Resources.Roles.MOBILE_USER);


    @NotNull
    @Size(min = 1, max = 64)
    @Column(unique = true,nullable = false, name = "username")
    @Access(AccessType.PROPERTY)
    private String username;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(unique = true,nullable = false, name="access_token")
    private String accessToken;

    @Column(unique = true,nullable = false,name = "swagger_key")
    private String swaggerKey;

    @Column(name = "auto_created")
    private boolean autoCreated;

    @Column(name = "created_by_platform")
    private Resources.Platforms createdByPlatform;

    private String role;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public boolean isAutoCreated() {
        return autoCreated;
    }

    public void setAutoCreated(boolean autoCreated) {
        this.autoCreated = autoCreated;
    }

    public Resources.Platforms getCreatedByPlatform() {
        return createdByPlatform;
    }

    public void setCreatedByPlatform(Resources.Platforms createdByPlatform) {
        this.createdByPlatform = createdByPlatform;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getAccToken() {
        return accessToken;
    }

    public void setAccToken(String accToken) {
        this.accessToken = accToken;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSwaggerKey() {
        return swaggerKey;
    }

    public void setSwaggerKey(String swaggerKey) {
        this.swaggerKey = swaggerKey;
    }

    public Set<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(Set<Recipe> recipes) {
        this.recipes = recipes;
    }

    public int getMaxSearchIngreds() {
        return maxSearchIngreds;
    }

    public void setMaxSearchIngreds(int maxSearchIngreds) {
        this.maxSearchIngreds = maxSearchIngreds;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }
}
