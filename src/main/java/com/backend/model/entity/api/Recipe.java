package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.backend.model.entity.account.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-14.
 */
@Entity
@NamedEntityGraph(name = "Recipe.find", attributeNodes = {
        @NamedAttributeNode(value = "partsIngreds"),
        @NamedAttributeNode(value = "partsCats")
})
@NamedQueries(
        @NamedQuery(name = "Recipe.findOne",query="select r from Recipe r where r.id = :id")
)
public class Recipe extends BaseEntity {

    @Column(name = "name",nullable = false,unique = true)
    private String name;

    @Column(name = "body")
    private String body;

    @Column(name = "alcohol_percent")
    private float alchoholPercent;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="author_id")
    private User author;

    // Read only
    @Column(name = "author_id", insertable = false,updatable = false)
    private Long authorId;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true, mappedBy = "recipe")
    private Set<PartIngredient> partsIngreds;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "recipe")
    private Set<PartCategory> partsCats;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PartIngredient> getPartsIngreds() {
        return partsIngreds;
    }

    public void setPartsIngreds(Set<PartIngredient> partsIngreds) {
        this.partsIngreds = partsIngreds;
    }

    public Set<PartCategory> getPartsCats() {
        return partsCats;
    }

    public void setPartsCats(Set<PartCategory> partsCats) {
        this.partsCats = partsCats;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public float getAlchoholPercent() {
        return alchoholPercent;
    }

    public void setAlchoholPercent(float alchoholPercent) {
        this.alchoholPercent = alchoholPercent;
    }

    public Long getAuthorId() {
        return authorId;
    }
}
