package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-13.
 */
@Entity
@NamedEntityGraph(name = "Category.ingredient", attributeNodes = {
        @NamedAttributeNode(value = "ingredientSet")
})
@NamedQueries(
        @NamedQuery(name = "Category.findAll",query="select distinct c from Category c")
)
public class Category extends BaseEntity {

    @Column(nullable = false,unique = true,name = "name")
    private String name;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY,mappedBy="categorySet")
    private Set<Ingredient> ingredientSet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Ingredient> getIngredientSet() {
        return ingredientSet;
    }

    public void setIngredientSet(Set<Ingredient> ingredientSet) {
        this.ingredientSet = ingredientSet;
    }
}
