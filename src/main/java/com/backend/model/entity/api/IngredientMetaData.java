package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

/**
 * Created by rasmus on 2016-05-28.
 */
@Entity
public class IngredientMetaData extends BaseEntity {

    //@ManyToOne
    //@JoinColumn(name = "vendor_id")
    //private Vendor vendor;

    @Column(name = "alcohol_percent")
    private double alcoholPercent;

    @OneToOne
    @MapsId
    private Ingredient ingredient;

    public double getAlchoholPercent() {
        return alcoholPercent;
    }

    public void setAlchoholPercent(double alchoholPercent) {
        this.alcoholPercent = alchoholPercent;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
}
