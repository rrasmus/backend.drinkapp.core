package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-14.
 */
@Entity
@NamedEntityGraph(name = "Ingredient.category", attributeNodes = {
        @NamedAttributeNode(value = "categorySet")
})
@NamedQueries(
        @NamedQuery(name = "Ingredient.findAll",query="select distinct i from Ingredient i")
)
public class Ingredient extends BaseEntity {
    @Column(name = "name",nullable = false,unique = true)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="Ingredient_Category")
    private Set<Category> categorySet;

    @OneToOne(mappedBy = "ingredient",fetch = FetchType.LAZY)
    private IngredientMetaData metaData;

    private String templateSrc;

    //@OneToMany(mappedBy = "ingredient")
    //@JsonManagedReference
    //private Set<IngredientImageData> ingredientImageDatas;


    public Set<Category> getCategorySet() {
        return categorySet;
    }

    public void setCategorySet(Set<Category> categorieSet) {
        this.categorySet = categorieSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IngredientMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(IngredientMetaData metaData) {
        this.metaData = metaData;
    }

    public String getTemplateSrc() {
        return templateSrc;
    }

    public void setTemplateSrc(String templateSrc) {
        this.templateSrc = templateSrc;
    }

    /*
    public Set<IngredientImageData> getIngredientImageDatas() {
        return ingredientImageDatas;
    }

    public void setIngredientImageDatas(Set<IngredientImageData> ingredientImageDatas) {
        this.ingredientImageDatas = ingredientImageDatas;
    }
    */
}
