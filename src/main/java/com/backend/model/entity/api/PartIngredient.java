package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.backend.model.util.Unit;
import com.backend.util.serializer.PartIngredientSerializer;
import com.backend.util.serializer.UserSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Created by rasmus on 2016-05-14.
 */
@Entity
public class PartIngredient extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ingredient_id",nullable = false)
    private Ingredient ingredient;

    // Read only column
    @Column(name = "ingredient_id", insertable = false,updatable = false)
    private Long ingredientId;

    @Column(name = "quantity")
    private float quantity;

    private Unit unit;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Long getIngredientId() {
        return ingredientId;
    }
}
