package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * Created by rasmus on 2016-05-13.
 */
@Entity
public class Vendor extends BaseEntity {

    @Column(nullable = false,unique = true,name = "name")
    private String name;

    //@OneToMany
    //@JoinColumn
    //private Set<IngredientMetaData> metaDataSet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
