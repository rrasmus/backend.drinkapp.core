package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

/**
 * Created by root on 6/26/16.
 *
 * Class used to upload image label connected to a certain ingredient when trying to find a match!
 */
@Entity
public class IngredientImageData extends BaseEntity {

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @Column(name = "image_url")
    private String imageUrl;

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
