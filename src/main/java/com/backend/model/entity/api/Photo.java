package com.backend.model.entity.api;

import com.backend.model.entity.BaseEntity;
import com.backend.model.entity.BaseTime;
import com.backend.model.entity.account.User;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rasmus on 12/26/16.
 */
@Entity
public class Photo extends BaseEntity {

    private String link;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    private Long result;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }
}
