package com.backend.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiParam;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by rasmus on 2016-05-13.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@MappedSuperclass
public class BaseEntity extends BaseTime {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*
    @Override
    public int hashCode() {
        return Math.toIntExact(this.id);
    }

    @Override
    public boolean equals(Object obj) {
        return hashCode() == obj.hashCode();
    }
    */
}