package com.backend.model.entity;

/**
 * Created by rasmus on 2016-06-03.
 */

import io.swagger.annotations.ApiParam;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by rasmus on 2016-06-03.
 */
@MappedSuperclass
public class BaseTime implements Serializable {

    @ApiParam(access = "internal")
    @Column(name = "created_at")
    private Long createdAt;
    @ApiParam(access = "internal")
    @Column(name = "latest_modified")
    private Long latestModified;

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLatestModified() {
        return latestModified;
    }

    public void setLatestModified(Long latestModified) {
        this.latestModified = latestModified;
    }

    @PrePersist
    public void createdAt() {
        this.createdAt = this.latestModified = new java.sql.Date(Calendar.getInstance().getTime().getTime()).getTime();
    }

    @PreUpdate
    public void updatedAt() {
        this.latestModified = new java.sql.Date(Calendar.getInstance().getTime().getTime()).getTime();
    }
}
