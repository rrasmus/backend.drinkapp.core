package com.backend.worker;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.backend.config.aws.AwsResources;
import com.backend.dao.PhotoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by rasmus on 12/27/16.
 */
public class AwsSqsReader extends Thread {

    private boolean running = true;

    //@Autowired
    private PhotoRepo photoRepo;

    //@Autowired
    private AWSCredentials awsCredentials;

    private AmazonSQSClient amazonSQSClient;

    public AwsSqsReader() {
        super();
        amazonSQSClient = new AmazonSQSClient(awsCredentials);
        amazonSQSClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
    }

    @Override
    public void run() {
        while (running) {
            System.out.println("Reading the sqs queue...");
            ReceiveMessageResult receiveMessageRequest = amazonSQSClient.receiveMessage(AwsResources.SQS.QUEUE_URL);
            List<Message> messageList = receiveMessageRequest.getMessages();
            System.out.println("Found " + messageList.size() + " messages");
            for (Message message : messageList) {
                System.out.println(message.toString());
            }
        }
    }

    public void stopRunner() {
        running = false;
    }
}
